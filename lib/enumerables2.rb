require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.include?(substring)
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  i = 0
  while i < string.length
    if string[i] not in arr
      arr << string[i]
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  new_arr = []
  longest = ""
  long = ""
  words = string.split(" ").delete!("!,.?")
  words.each do |word|
    if word.length > longest
      longest = word
    end
  end
  longest
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alp = [*"a".."z"]
  new_arr = []
  string.each_char do |char|
    new_arr << char unless new_arr.include?(char)
  end
  alp.each_char do |char|
    if char new_arr

end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  range = (first_yr..last_yr)
  range.each do |year|
    while true
      if not_repeat_year(year)
        new_arr << year
      end
    end
  end
  new_arr
end

def not_repeat_year?(year)
  if year.split("") == year.split("").uniq
    return true
  end
  false
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
end

def no_repeats?(song_name, songs)
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  string.delete!("?.,!")
  words = string.split(" ")
  dist = 0
  result = ""
  words.each do |word|
    if dist > c_distance
      dist = c_distance
      result = word
    end
  end
  result
end

def c_distance(word)
  word.rindex("c")
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  arr.each_with_index do |el, indx|
    if arr[indx] == arr[indx + 1]
      chunk << indx
    else
      chunk[1] = 
end
